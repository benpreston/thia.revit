﻿using System;
using System.Collections.Generic;
using Thia.Revit.Core.Extensions;

namespace Thia.Revit.Core
{

    public class BuilderFactory
    {

        private readonly Dictionary<BIMObjectType, Func<IBuilder>> typedConstructors;


        public BuilderFactory()
        {
            typedConstructors = new Dictionary<BIMObjectType, Func<IBuilder>>();

            foreach (BIMObjectType type in EnumExts.GetValues<BIMObjectType>())
            {
                Type targetType = type.GetTargetType();

                if (targetType != null)
                {
                    typedConstructors[type] = () => (BuilderBase)Activator.CreateInstance(targetType);
                }
            }

        }


        public IBuilder this[BIMObjectType type] => CreateBuilder(type);


        public IBuilder CreateBuilder(BIMObjectType type) => typedConstructors[type]();


    }


}
