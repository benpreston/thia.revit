﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;
using Application = Autodesk.Revit.ApplicationServices.Application;

namespace Thia.Revit.Core
{
    public static class DocumentUtility
    {

        /// <summary>
        /// Creates a new Revit project document and saves to a specified location.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="newFilePath"></param>
        /// <returns>A reference to the newly created document.</returns>
        public static Document CreateProjectDocument(Application app, string newFilePath, bool saveToDisk = true, UnitSystem unitSystem = UnitSystem.Imperial)
        {
            Document newDoc = app.NewProjectDocument(unitSystem);

            if (saveToDisk)
                newDoc.SaveAs(newFilePath);

            return newDoc;
        }

    }
}
