﻿
using Autodesk.Revit.DB;

namespace Thia.Revit.Core
{
    public abstract class BuilderBase : IBuilder
    {
        /// <summary>
        /// The data will be parsed from the input JSON.
        /// </summary>
        public IO.InputElementData Data { get; protected set; }


        /// <summary>
        /// Base constructor;
        /// </summary>
        public BuilderBase() : this(null) { }


        /// <summary>
        /// Constructor with Data.
        /// </summary>
        /// <param name="targetDocument">The Revit Document in which the Build action takes place</param>
        /// <param name="data">Data parsed from the input JSON</param>
        public BuilderBase(IO.InputElementData data)
        {
            this.Data = data;
        }


        /// <summary>
        /// Sets the data for the builder object;
        /// </summary>
        /// <param name="data"></param>
        public virtual void SetData(IO.InputElementData data)
        {
            this.Data = data;
        }


        /// <summary>
        /// The method triggered prior to a transaction being opened.
        /// </summary>
        public virtual void PreBuild(Document targetDocument) { }


        /// <summary>
        /// The method triggered within an open transaction.
        /// </summary>
        public virtual void Build(Document targetDocument) { }


        /// <summary>
        /// The method triggered after a transaction is closed.
        /// </summary>
        public virtual void PostBuild(Document targetDocument) { }

    }
}
