﻿using System.Collections.Generic;
using System.Linq;

using Autodesk.Revit.DB;


namespace Thia.Revit.Core
{
    class FloorBuilder : BuilderBase
    {

        private List<(ElementId, List<CurveLoop>)> levelCurveTuples;

        public FloorBuilder() : this(null) { }


        public FloorBuilder(IO.InputElementData data) : base(data)
        {
            this.levelCurveTuples = new List<(ElementId, List<CurveLoop>)>();
        }


        public override void Build(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;


            if (this.levelCurveTuples == null || this.levelCurveTuples.Count < 1)
                return;

            ElementId floorTypeId =
                new FilteredElementCollector(targetDocument)
                .OfCategory(BuiltInCategory.OST_Floors)
                .WhereElementIsElementType()
                .ToElementIds()
                .First();


            foreach ((ElementId, List<CurveLoop>) tuple in levelCurveTuples)
            {
                Floor.Create(targetDocument, tuple.Item2, floorTypeId, tuple.Item1);
            }
        }



        public override void PreBuild(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;

            LevelUtility levels = new LevelUtility(targetDocument);


            if (levels.LevelAt(Data.BaseElevation, out Level baseLevel, createIfNonExistant: true))
            {
                // Currently, the wall creator only allows a single surface per wall
                foreach (Mesh mesh in Data.Geometry)
                    CreateCuveLoops(mesh, baseLevel);
            }
        }


        protected void CreateCuveLoops(Mesh mesh, Level baseLevel)
        {
            List<Curve> orderedCurves = new List<Curve>();
            Vector3[] orderedBounds = mesh.OrderedBoundaryPoints;

            for (int i = 0; i < orderedBounds.Length; i++)
            {
                XYZ point1 = orderedBounds[i];
                XYZ point2 = i == orderedBounds.Length - 1 ? orderedBounds[0] : orderedBounds[i + 1];

                Autodesk.Revit.DB.Line line = Autodesk.Revit.DB.Line.CreateBound(point1, point2);
                orderedCurves.Add(line);
            }

            CurveLoop loop = CurveLoop.Create(orderedCurves);
            this.levelCurveTuples.Add((baseLevel.Id, new List<CurveLoop>() { loop }));
        }

    }
}
