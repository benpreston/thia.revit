﻿
namespace Thia.Revit.Core
{
    public enum TransactionType
    {
        NONE = 0,
        COMMIT = 100,
        ROLLBACK = 200
    }
}
