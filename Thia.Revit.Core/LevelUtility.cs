﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

using Thia.Revit.Core.Extensions;

namespace Thia.Revit.Core
{
    class LevelUtility
    {
        Document _doc;
        IEnumerable<Level> _levels;

        public LevelUtility(Document doc)
        {
            _doc = doc;
            _levels = new FilteredElementCollector(_doc)
                .OfClass(typeof(Level))
                .WhereElementIsNotElementType()
                .Cast<Level>();
        }


        public bool NearestLevelBelow(double elevation, out Level level)
        {
            IEnumerable<Level> orderedLevels = _levels.OrderBy(l => l.Elevation).Reverse();

            Level firstLevelBelowElevation = _levels.FirstOrDefault(l => l.Elevation - elevation < 0.0);

            if (firstLevelBelowElevation == null)
            {
                level = null;
                return false;
            }

            level = firstLevelBelowElevation;
            return true;
        }

        public bool NearestLevelAbove(double elevation, out Level level)
        {
            IEnumerable<Level> orderedLevels = _levels.OrderBy(l => l.Elevation);

            Level firstLevelAboveElevation = _levels.FirstOrDefault(l => elevation - l.Elevation < 0.0);

            if (firstLevelAboveElevation == null)
            {
                level = null;
                return false;
            }

            level = firstLevelAboveElevation;
            return true;
        }

        public bool LevelAt(double elevation, out Level level, bool createIfNonExistant = false)
        {
            Level levelAtElevation = _levels.FirstOrDefault(l => Math.Abs(elevation - l.Elevation) < double.Epsilon);

            if (levelAtElevation != null)
            {
                level = levelAtElevation;
                return true;
            }

            else if (createIfNonExistant)
            {
                Level newLevel = null;

                Action<Document> action = (_doc) => newLevel = Level.Create(_doc, elevation);

                this._doc.WithTransaction("Create new level", action);

                level = newLevel;
                return true;
            }


            level = null;
            return false;
        }



    }
}
