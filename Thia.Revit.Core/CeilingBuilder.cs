﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;

namespace Thia.Revit.Core
{
    class CeilingBuilder : BuilderBase
    {
        private List<(ElementId, List<CurveLoop>, double)> levelCurveTuples;

        public CeilingBuilder() : this(null) { }

        public CeilingBuilder(IO.InputElementData data) : base(data)
        {
            this.levelCurveTuples = new List<(ElementId, List<CurveLoop>, double)>();
        }

        public override void Build(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;


            if (this.levelCurveTuples == null || this.levelCurveTuples.Count < 1)
                return;

            ElementId ceilingTypeId =
                new FilteredElementCollector(targetDocument)
                .OfCategory(BuiltInCategory.OST_Ceilings)
                .WhereElementIsElementType()
                .ToElementIds()
                .First();


            foreach ((ElementId, List<CurveLoop>, double) tuple in levelCurveTuples)
            {
                Ceiling newCeiling = Ceiling.Create(targetDocument, tuple.Item2, ceilingTypeId, tuple.Item1);

                Parameter ceilingHeighParam = newCeiling.get_Parameter(BuiltInParameter.CEILING_HEIGHTABOVELEVEL_PARAM);

                ceilingHeighParam.Set(tuple.Item3);
            }
        }



        public override void PreBuild(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;

            LevelUtility levels = new LevelUtility(targetDocument);


            if (levels.LevelAt(Data.BaseElevation, out Level baseLevel))
            {
                // Currently, the wall creator only allows a single surface per wall
                foreach (Mesh mesh in Data.Geometry)
                {
                    CreateCuveLoops(mesh, baseLevel);
                }
            }
        }


        protected void CreateCuveLoops(Mesh mesh, Level baseLevel)
        {
            List<Curve> orderedCurves = new List<Curve>();

            for (int i = 0; i < mesh.Vertices.Length; i++)
            {
                XYZ point1 = mesh.Vertices[i];
                XYZ point2 = i == mesh.Vertices.Length - 1 ? mesh.Vertices[0] : mesh.Vertices[i + 1];

                Autodesk.Revit.DB.Line line = Autodesk.Revit.DB.Line.CreateBound(point1, point2);
                orderedCurves.Add(line);
            }

            CurveLoop loop = CurveLoop.Create(orderedCurves);

            //Sloped ceilings are not supported
            this.levelCurveTuples.Add((baseLevel.Id, new List<CurveLoop>() { loop }, mesh.Vertices[0].Z));
        }
    }
}
