﻿using System.Collections.Generic;
using System.IO;
using Autodesk.Revit.DB;


namespace Thia.Revit.Core
{

    public class WallBuilder : BuilderBase
    {

        private List<(Curve, ElementId)> levelCurveTuples;


        public WallBuilder() : this(null) { }


        public WallBuilder(IO.InputElementData data) : base(data)
        {
            this.levelCurveTuples = new List<(Curve, ElementId)>();
        }



        public override void Build(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;


            if (this.levelCurveTuples == null || this.levelCurveTuples.Count < 1)
                return;


            foreach ((Curve, ElementId) tuple in levelCurveTuples)
            {
                Wall.Create(targetDocument, tuple.Item1, tuple.Item2, false);
            }
        }



        public override void PreBuild(Document targetDocument)
        {
            if (this.Data == null || targetDocument == null)
                return;


            FilteredElementCollector levelCollector = new FilteredElementCollector(targetDocument).OfClass(typeof(Level));
            ElementId someLevelId = levelCollector.FirstElementId();


            if (someLevelId == null || someLevelId.IntegerValue < 0) 
                throw new InvalidDataException("ElementID is invalid.");

            Vector3 point1 = Data.Geometry[0][0];
            Vector3 point2 = Data.Geometry[0][1];

            XYZ start = new XYZ(point1.X, point1.Y, point1.Z);
            XYZ end = new XYZ(point2.X, point2.Y, point2.Z);


            (Curve, ElementId) newTuple = (Autodesk.Revit.DB.Line.CreateBound(start, end), someLevelId);


            levelCurveTuples.Add(newTuple);
        }
    }
}
