﻿using Autodesk.Revit.DB;

namespace Thia.Revit.Core
{
    public class Vector3
    {
        public Vector3() { }

        public Vector3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 Zero => new Vector3(0, 0, 0);



        public double X { get; set; } = 0.0;

        public double Y { get; set; } = 0.0;

        public double Z { get; set; } = 0.0;

        public static implicit operator Vector3(XYZ point) =>
            new Vector3(point.X, point.Y, point.Z);

        public static implicit operator XYZ(Vector3 v3) =>
            new XYZ(v3.X, v3.Y, v3.Z);

        public static Vector3 operator +(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
    }
}
