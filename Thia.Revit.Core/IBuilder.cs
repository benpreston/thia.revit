﻿using Autodesk.Revit.DB;


namespace Thia.Revit.Core
{
    public interface IBuilder
    {
        void SetData(IO.InputElementData data);
        void PreBuild(Document targetDocument);
        void Build(Document targetDocument);
        void PostBuild(Document targetDocument);
    }
}
