﻿
namespace Thia.Revit.Core.IO
{
    public class InputElementDataCollection
    {
        public int CommandId { get; set; }
        public InputElementData[] Elements { get; set; }
    }
}
