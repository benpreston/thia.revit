﻿using System;
using System.IO;

namespace Thia.Revit.Core.IO
{
    public static class FileUtils
    {

        public static bool TryRead(string filePath, out string output)
        {
            output = string.Empty;
            bool success = false;

            try
            {
                if (!File.Exists(filePath))
                    return false;

                output = File.ReadAllText(filePath);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception reading the json file: " + ex);
                success = false;
            }

            return success;
        }

    }
}
