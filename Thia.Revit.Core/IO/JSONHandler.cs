﻿using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;

namespace Thia.Revit.Core.IO
{
    public class JSONHandler
    {
		private static string _schemaText = @"{
	'$schema': 'https://json-schema.org/draft/2020-12/schema',
	'$id': 'CommandData#',
	'title': 'CommandData',
	'description': 'Contains information for creating one or more BIM elements.',
	'type': 'object',
	'properties': {
		'commandId': {
			'description': 'an identifier for this document',
			'type': 'integer'
		},
		'elements': {
			'type': 'array',
			'items': {
				'description': 'a list of element data',
				'$ref': '#/$defs/inputElementData'
			},
		}
	},
	'required': ['commandId', 'elements'],
	'$defs': {
		'inputElementData': {
			'type': 'object',
			'properties': {
				'elemId': {
					'description': 'The unique identifier for an element',
					'type': 'integer'
				},
				'elemType': {
					'description': 'the type of BIM element to be created',
					'type': 'string'
				},
				'baseElevation': {
					'description': 'The elevation of the base horizontal datum for this element. For floors and ceilings, this will be equal to the Z-value of the element. For walls, it will be the Z-value of the base of the wall.',
					'type': 'number'
				},
				'geometry': {
					'description': 'The meshed geometry of the element',
					'type': 'array',
					'items': {
						'description': 'mesh geometry',
						'$ref': '#/$defs/mesh'
					},
					'minItems': 1
				}
			},
			'required': ['elemId', 'baseElevation', 'geometry']
	},
		'mesh': {
			'description': 'A surface represented by a mesh',
			'type': 'object',
			'properties': {
				'solid': {
					'description': 'determines whether this element is a solid geometry or a void geometry',
					'type': 'boolean'
				},
				'vertices': {
	'type': 'array',
					'items': {
		'description': 'the vertices of the mesh',
						'$ref': '#/$defs/vector'
					}
},
				'triangles': {
	'type': 'array',
					'items': {
		'description': 'an ordered array representing the triangles of the mesh',
						'type': 'integer',
						'minimum': 0
					}
},
				'normals': {
	'type': 'array',
					'items': {
		'description': 'the normals of the mesh',
						'$ref': '#/$defs/vector'
					}
},
			},
			'required': ['vertices', 'triangles']
		},
		'vector': {
	'description': 'An point or vector',
			'type': 'object',
			'properties': {
		'x': {
			'description': 'The X value',
					'type': 'number'
				},
				'y': {
			'description': 'The Y value',
					'type': 'number'
				},
				'z': {
			'description': 'The Z value',
					'type': 'number'
				}
	},
			'required': ['x', 'y', 'z']
		}
	}
}";

		private JSchema _schema;
		private readonly string _jsonText;
        private readonly JObject _json;

        public bool IsValid { get; private set; }

        public JSONHandler(string jsonText)
        {
            if (_schema == null)
            {
				_schema = JSchema.Parse(_schemaText);
            }

            _jsonText = jsonText;

            _json = JObject.Parse(_jsonText);

			//IsValid = _json.IsValid(_schema);
        }

		public T Parse<T>() => JsonConvert.DeserializeObject<T>(_jsonText);

		public T Parse<T>(string value) => JsonConvert.DeserializeObject<T>(value);

		public string GetChild(string key)
        {
			return _json[key].ToString();
        }

    }
}
