﻿
namespace Thia.Revit.Core.IO
{

    public class InputElementData
    {
        public int ElemId { get; set; }
        public string ElemType { get; set; }
        public BIMObjectType BIMObjectType { get; set; }
        public double BaseElevation { get; set; }
        public Mesh[] Geometry { get; set; }
    }
}
