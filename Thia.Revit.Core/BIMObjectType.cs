﻿using System.ComponentModel;
using Thia.Revit.Core.Attributes;

namespace Thia.Revit.Core
{
    public enum BIMObjectType
    {
        [TargetBuilderType(null)]
        [Description("none")]
        NONE = 0,

        [TargetBuilderType(typeof(WallBuilder))]
        [Description("wall")]
        WALL = 100,

        [TargetBuilderType(typeof(FloorBuilder))]
        [Description("floor")]
        FLOOR = 200,

        [TargetBuilderType(typeof(CeilingBuilder))]
        [Description("ceiling")]
        CEILING = 300
    }
}
