﻿namespace Thia.Revit.Core
{
    public class Line
    {
        public Vector3 Start { get; set; }

        public Vector3 End { get; set; }
    }
}
