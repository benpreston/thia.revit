﻿using System;

namespace Thia.Revit.Core.Attributes
{
    public class TargetBuilderTypeAttribute : System.Attribute
    {
        public Type TargetBuilderType;
        public readonly bool IsValid;


        /// <summary>
        /// Ties the required Builder and BuilderData types to the BIMObjectType Enum.
        /// </summary>
        /// <param name="builderType">Must inherit from BuilderBase</param>
        /// <param name="dataType">Must implement IBuilderData</param>
        public TargetBuilderTypeAttribute(Type builderType)
        {
            if (builderType == null)
            {
                IsValid = false;
                TargetBuilderType = null;
            }

            else
            {
                IsValid = true;
                TargetBuilderType = builderType;
            }
        }
    }
}
