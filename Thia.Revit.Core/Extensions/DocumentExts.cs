﻿using System;
using System.Diagnostics;
using Autodesk.Revit.DB;


namespace Thia.Revit.Core.Extensions
{

    public static class DocumentExts 
    {

        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> buildAction
            ) => WithTransaction(doc, transactionName, null, buildAction, null, TransactionType.COMMIT, null);


        public static TransactionStatus WithTransaction(
            this Document doc,
            string transactionName,
            Action<Document> buildAction,
            TransactionType transactionType
        ) => WithTransaction(doc, transactionName, null, buildAction, null, transactionType, null);


        public static TransactionStatus WithTransaction(
            this Document doc,
            string transactionName,
            Action<Document> buildAction,
            TransactionType transactionType,
            FailureHandlingOptions failureHandlingOptions
        ) => WithTransaction(doc, transactionName, null, buildAction, null, transactionType, failureHandlingOptions);


        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction
            ) => WithTransaction(doc, transactionName, preBuildAction, buildAction, null, TransactionType.COMMIT, null);


        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction,
                TransactionType transactionType
            ) => WithTransaction(doc, transactionName, preBuildAction, buildAction, null, transactionType, null);


        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction,
                TransactionType transactionType,
                FailureHandlingOptions failureHandlingOptions
            ) => WithTransaction(doc, transactionName, preBuildAction, buildAction, null, transactionType, failureHandlingOptions);


        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction,
                Action<Document> postBuildAction
            ) => WithTransaction(doc, transactionName, preBuildAction, buildAction, postBuildAction, TransactionType.COMMIT, null);


        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction,
                Action<Document> postBuildAction,
                TransactionType transactionType
            ) => WithTransaction(doc, transactionName, preBuildAction, buildAction, postBuildAction, transactionType, null);



        public static TransactionStatus WithTransaction(
                this Document doc,
                string transactionName,
                Action<Document> preBuildAction,
                Action<Document> buildAction,
                Action<Document> postBuildAction,
                TransactionType transactionType,
                FailureHandlingOptions failureHandlingOptions
        )
        {

            TransactionStatus status = TransactionStatus.Uninitialized;


            if (preBuildAction != null)
            {
                try
                {
                    preBuildAction(doc);
                }
                catch (Exception e)
                {
                    Debug.Write(e);
                    return status;
                }
            }


            using (var transaction = new Transaction(doc, transactionName))
            {
                try
                {
                    TransactionStatus txStatus = transaction.Start();
                    if (txStatus != TransactionStatus.Started)
                        return txStatus;

                    buildAction(doc);

                    status = transaction.End(transactionType);
                }
                catch
                {
                    transaction.End(TransactionType.ROLLBACK, failureHandlingOptions);
                    throw;
                }
            }


            if (postBuildAction != null)
            { 
                try
                {
                    postBuildAction(doc);
                }
                catch (Exception e)
                {
                    Debug.Write(e);
                }
            }

            return status;
        }
    }
}
