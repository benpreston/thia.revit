﻿using System;
using System.Diagnostics;

using Autodesk.Revit.DB;

namespace Thia.Revit.Core.Extensions
{
    internal static class TransactionExts
    {

        public static TransactionStatus End(
            this Transaction transaction,
            TransactionType transactionType,
            FailureHandlingOptions failureHandlingOptions = null)
        {
            TransactionStatus status = transaction.GetStatus();

            if (status != TransactionStatus.Started)
                return status;

            try
            {
                switch(transactionType)
                {
                    case TransactionType.COMMIT:
                        return failureHandlingOptions == null ? transaction.Commit() : transaction.Commit(failureHandlingOptions);

                    case TransactionType.ROLLBACK:
                    case TransactionType.NONE:
                    default:
                        return failureHandlingOptions == null ? transaction.RollBack() : transaction.RollBack(failureHandlingOptions);
                }
            }

            catch(Exception e)
            {
                //TODO: add logger
                Debug.Print(e.Message);
            }

            return TransactionStatus.Error;
        }

    }
}
