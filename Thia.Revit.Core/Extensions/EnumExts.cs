﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Thia.Revit.Core.Attributes;


namespace Thia.Revit.Core.Extensions
{

    public static class EnumExts
    {
        private static bool TryGetAttributes<T>(this T enumValue, Type attributeType, out object attribute)
        {
            attribute = null;
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(attributeType, true);

                if (attrs != null && attrs.Length > 0)
                {
                    attribute = attrs[0];
                    return true;
                }
            }

            return false;
        }



        public static Type GetTargetType<T>(this T enumValue)
        {
            if (typeof(T).IsEnum == false)
                return null;


            if (enumValue.TryGetAttributes(typeof(TargetBuilderTypeAttribute), out object attribute))
            {
                TargetBuilderTypeAttribute attr = (TargetBuilderTypeAttribute)attribute;

                if (attr.IsValid)
                    return attr.TargetBuilderType;
            }

            return null;
        }



        public static string GetDescription<T>(this T enumValue) where T : struct, IConvertible
        {
            if (typeof(T).IsEnum == false)
                return null;

            var description = enumValue.ToString();
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    description = ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return description;
        }

        public static IEnumerable<T> GetValues<T>() => 
            Enum.GetValues(typeof(T)).Cast<T>();
    }
}
