﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Thia.Revit.Core
{
    public class Mesh
    {
        public Vector3 this[int i]
        {
            get
            {
                if (this.Vertices == null)
                    return null;

                if (this.Vertices.Length < 1)
                    return null;

                if (i < 0 || i >= this.Vertices.Length)
                    return null;

                return this.Vertices[i];
            }
        }
        public Vector3[] Vertices { get; set; }
        public Vector3[] Normals { get; set; }
        public int[] Triangles { get; set; }
        public int TriangleCount => Triangles == null ? 0 : Triangles.Length / 3;



        private bool boundaryPointsCalced = false;
        private Vector3[] orderedBoundaryPoints;
        public Vector3[] OrderedBoundaryPoints
        {
            get
            {
                if (boundaryPointsCalced == false)
                {
                    orderedBoundaryPoints = CalcBoundaryPoints();
                    boundaryPointsCalced = true;
                }

                return orderedBoundaryPoints;
            }
        }


        private bool edgesCalced = false;
        private (int, int)[] edges;

        /// <summary>
        /// All edges in the mesh. Includes duplicates for abutting triangles.
        /// </summary>
        public (int, int)[] Edges
        {
            get
            {
                if (edgesCalced == false)
                {
                    edges = CalcEdges();
                    edgesCalced = true;
                }

                return edges;
            }
        }


        private bool uniqueEdgesCalced = false;
        private (int, int)[] uniqueEdges;

        /// <summary>
        /// Unique edges of the mesh. Does not include duplicates for abutting triangles.
        /// </summary>
        public (int, int)[] UniqueEdges
        {
            get
            {
                if (uniqueEdgesCalced == false)
                {
                    uniqueEdges = CalcUniqueEdges();
                    uniqueEdgesCalced = true;
                }

                return uniqueEdges;
            }
        }


        private bool isClosed;
        private bool isClosedCalced = false;
        public bool IsClosed
        {
            get
            {
                if (isClosedCalced == false)
                {
                    isClosed = Vertices.Length + (Triangles.Length / 3) - UniqueEdges.Length == 2;
                    isClosedCalced = true;
                }

                return isClosed;
            }
        }




        private (int, int)[] CalcUniqueEdges()
        {
            List<(int, int)> uniqEdges = new List<(int, int)>();

            // Create a list of unique edges in the array
            List<int> edgeIndicesToRemove = new List<int>();

            for (int i = 0; i < Edges.Length; i++)
            {
                for (int j = i + 1; j < Edges.Length; j++)
                {

                    int vi0 = Edges[i].Item1;
                    int vi1 = Edges[i].Item2;
                    int vj0 = Edges[j].Item1;
                    int vj1 = Edges[j].Item2;

                    // If edges are the same edge, we need to remove both of them, as they are not boundary edges
                    if ((vi0 == vj0 && vi1 == vj1) ||
                        (vi0 == vj1 && vi1 == vj0))
                    {
                        edgeIndicesToRemove.Add(i);
                        edgeIndicesToRemove.Add(j);
                    }
                }
            }

            for (int i = 0; i < Edges.Length; i++)
            {
                if (edgeIndicesToRemove.Contains(i) == false)
                {
                    uniqEdges.Add(Edges[i]);
                }
            }

            return uniqEdges.ToArray();
        }




        private (int, int)[] CalcEdges()
        {
            if (Triangles == null || TriangleCount < 1)
                throw new System.InvalidOperationException("Mesh does not contain any triangles.");


            // The number of edges is equal to the number of points in the Triangles array
            List<(int, int)> edges = new List<(int, int)>();


            // iterate through triangles to create pairs of point indices representing edges
            // this will create duplicates, as triangles that border each other share an edge
            for (int i = 0; i < Triangles.Length; i += 3)
            {
                edges.Add((Triangles[i], Triangles[i + 1]));
                edges.Add((Triangles[i + 1], Triangles[i + 2]));
                edges.Add((Triangles[i + 2], Triangles[i]));
            }

            return edges.ToArray();
        }





        /// <summary>
        /// Returns the boundary points of a mesh, if a boundary exists.
        /// </summary>
        /// <returns>A Vector3 array, starting at the vector with the lowest magnitude and moving clockwise around the face normal.</returns>
        private Vector3[] CalcBoundaryPoints()
        {
            if (Triangles == null || TriangleCount < 1)
                throw new System.InvalidOperationException("Mesh does not contain any triangles.");


            // Create map to order the indices
            Dictionary<int, List<int>> indexMap = new Dictionary<int, List<int>>();

            foreach ((int, int) edge in UniqueEdges)
            {
                if (indexMap.ContainsKey(edge.Item1) == false)
                    indexMap.Add(edge.Item1, new List<int>());

                indexMap[edge.Item1].Add(edge.Item2);

                if (indexMap.ContainsKey(edge.Item2) == false)
                    indexMap.Add(edge.Item2, new List<int>());

                indexMap[edge.Item2].Add(edge.Item1);
            }



            // Order indices
            int[] orderedIndices = new int[UniqueEdges.Length];

            for (int i = 0; i < orderedIndices.Length; i++)
            {
                orderedIndices[i] = -1;
            }

            int firstIndex = (UniqueEdges[0].Item1);
            int currentIndex = (UniqueEdges[0].Item2);

            orderedIndices[0] = firstIndex;
            orderedIndices[1] = currentIndex;

            for (int i = 2; i < UniqueEdges.Length; i++)
            {
                int nextIndex = indexMap[currentIndex].First(x => orderedIndices.Contains(x) == false);
                orderedIndices[i] = nextIndex;
                currentIndex = nextIndex;
            }

            // Return points based on ordered indices
            return orderedIndices.Select(x => Vertices[x]).ToArray();
        }

    }
}
