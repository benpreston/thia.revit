using DesignAutomationFramework;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using System;
using System.IO;
using Thia.Revit.Core.IO;
using Thia.Revit.Core;
using Thia.Revit.Core.Extensions;

namespace Thia.Revit.Forge
{
    [Regeneration(RegenerationOption.Manual)]
    [Transaction(TransactionMode.Manual)]
    class ThiaApplication : IExternalDBApplication
    {
        private BuilderFactory BuilderFactory;

        public ExternalDBApplicationResult OnStartup(ControlledApplication app)
        {
            DesignAutomationBridge.DesignAutomationReadyEvent += HandleDesignAutomationReadyEvent;
            return ExternalDBApplicationResult.Succeeded;
        }


        public ExternalDBApplicationResult OnShutdown(ControlledApplication app)
        {
            return ExternalDBApplicationResult.Succeeded;
        }


        public void HandleDesignAutomationReadyEvent(object sender, DesignAutomationReadyEventArgs e)
        {
            e.Succeeded = true;
            ProcessAutomationRequest(e.DesignAutomationData);
        }



        public static void ProcessAutomationRequest(DesignAutomationData data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            Application rvtApp = data.RevitApp;
            if (rvtApp == null)
                throw new InvalidDataException(nameof(rvtApp));


            string modelPath = data.FilePath;
            if (String.IsNullOrWhiteSpace(modelPath)) 
                throw new InvalidDataException(nameof(modelPath));


            Document doc = data.RevitDoc;

            if (GetOnDemandFile())
            {
                string jsonText = File.ReadAllText("instructions.json");

                Console.WriteLine(jsonText);

                InputElementDataCollection collection = null;

                try
                {
                    JSONHandler jsonHandler = new JSONHandler(jsonText);
                    collection = jsonHandler.Parse<InputElementDataCollection>(jsonHandler.GetChild("instruction"));
                    Console.WriteLine("Parsed: " + collection.ToString());
                }

                catch(Exception e)
                {
                    Console.WriteLine("ERROR ------ ");
                    Console.WriteLine(e.Message);
                    return;
                }

                //JSONHandler jsonHandler = new JSONHandler(jsonText);

                //if (jsonHandler.IsValid == false)
                //{
                //    Console.WriteLine("Invalid JSON input. Returning early.");
                //    return;
                //}

                //InputDataCollection inputData = jsonHandler.Parse<InputDataCollection>();

                //if (inputData == null)
                //{
                //    Console.WriteLine("Could not parse JSON. Returning early");
                //}

                if (collection != null)
                {
                    BuildBIM(doc, collection);
                }
            }
        }




        private static void BuildBIM(Document doc, InputElementDataCollection collection)
        {
            BuilderFactory factory = new BuilderFactory();

            foreach (InputElementData inputData in collection.Elements)
            {
                inputData.BIMObjectType = (BIMObjectType)Enum.Parse(typeof(BIMObjectType), inputData.ElemType.ToUpper());
                IBuilder bimObjectBuilder = factory.CreateBuilder(inputData.BIMObjectType);
                bimObjectBuilder.SetData(inputData);

                doc.WithTransaction("Build from JSON", bimObjectBuilder.PreBuild, bimObjectBuilder.Build);
            }

            ModelPath path = ModelPathUtils.ConvertUserVisiblePathToModelPath("result.rvt");

            doc.SaveAs(path, new SaveAsOptions());
        }




        private static bool GetOnDemandFile()
        {
            Console.WriteLine("!ACESAPI:acesHttpOperation(instructionData,{1},{2},{3},{4})", "instructionData", "", "", "", "file://instructions.json");

            while (true)
            {
                char ch = Convert.ToChar(Console.Read());

                int idx = 0;

                if (ch == '\x3')
                {
                    return false;
                }    
                else if (ch == '\n')
                {
                    return true;
                }

                if (idx >= 512)
                {
                    return false;
                }
                idx++;
            }

        }



    }
}
