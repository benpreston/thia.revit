﻿using System;

namespace Thia.Revit.Desktop.UI
{
    [System.AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class MountableAttribute : Attribute
    {
        public string ButtonText;
        public MountableAttribute(string buttonText)
        {
            ButtonText = buttonText;
        }
    }
}
