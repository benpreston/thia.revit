﻿using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Thia.Revit.Desktop.UI
{
    public static class FileFolderBrowser
    {
        public static string GetUserDefinedPath()
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
                return sfd.FileName;

            return null;
        }

        public static string GetFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
                return ofd.FileName;

            return null;
        }

        public static string SelectFolder()
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                return dialog.FileName;
            }

            return string.Empty;
        }

    }
}
