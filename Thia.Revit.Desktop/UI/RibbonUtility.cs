﻿using System;
using System.Resources;
using System.Drawing;

using Autodesk.Revit.UI;


namespace Thia.Revit.Desktop.UI
{
    public class RibbonUtility
    {
        public string ApplicationPath { get; private set; }
        public ResourceManager ResourceManager { get; private set; }


        public RibbonUtility(string applicationPath, ResourceManager resourceManager)
        {
            this.ApplicationPath = applicationPath;
            this.ResourceManager = resourceManager;
        }



        private bool CreateButtonData(Type type, out PushButtonData data)
        {
            MountableAttribute mountable = Attribute.GetCustomAttribute(type, typeof(MountableAttribute)) as MountableAttribute;

            // Check to make sure that t is IExternalCommand and has the Mountable attribute
            if (typeof(IExternalCommand).IsAssignableFrom(type) && mountable != null)
            {

                string fullName = type.FullName;
                string className = type.Name;

                data = new PushButtonData(
                    "cmd" + className,
                    mountable.ButtonText,
                    ApplicationPath,
                    fullName
                    );


                if (typeof(IExternalCommandAvailability).IsAssignableFrom(type))
                {
                    data.AvailabilityClassName = fullName;
                }

                if (this.ResourceManager != null)
                {
                    Bitmap bitmap = ResourceManager.GetObject("Icon_" + className) as Bitmap;

                    if (bitmap != null)
                    {
                        data.LargeImage = IconUtility.GetImage(bitmap.GetHbitmap());
                    }
                }

                return true;
            }

            data = null;
            return false;

        }

        /// <summary>
        /// Create a new button on the Revit application ribbon.
        /// </summary>
        /// <param name="type">The TypeOf IExternalCommand that is to be mounted to the ribbon panel</param>
        /// <param name="panel"></param>
        /// <param name="addFollowingSeparator"></param>
        public void MountButton(
            Type type,
            RibbonPanel panel,
            bool addFollowingSeparator = false)
        {
            if (CreateButtonData(type, out PushButtonData d))
            {
                panel.AddItem(d);

                if (addFollowingSeparator)
                    panel.AddSeparator();
            }
        }


        /// <summary>
        /// Create up to 3 buttons on the Revit application ribbon. 
        /// </summary>
        /// <param name="types">Minimum of 2, maximum of 3 Types supported. Type must inherit from IExternalCommand. Fails silently if more than 3 are input.</param>
        /// <param name="panel"></param>
        /// <param name="addFollowingSeparator"></param>
        public void MountStackedButtons(
            Type[] types,
            RibbonPanel panel,
            bool addFollowingSeparator = false)
        {
            if (types.Length < 2)
                return;

            PushButtonData d0, d1, d2;

            if (CreateButtonData(types[0], out d0) && CreateButtonData(types[1], out d1))
            {

                if (types.Length > 2)
                {
                    if (CreateButtonData(types[2], out d2))
                    {
                        panel.AddStackedItems(d0, d1, d2);
                    }
                }

                else
                {
                    panel.AddStackedItems(d0, d1);
                }

                if (addFollowingSeparator)
                    panel.AddSeparator();

            }
        }
    }
}
