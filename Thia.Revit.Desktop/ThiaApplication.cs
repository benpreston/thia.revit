using System.Reflection;
using Autodesk.Revit.UI;
using Thia.Revit.Core;
using Thia.Revit.Desktop.UI;

namespace Thia.Revit.Desktop
{
    class ThiaApplication : IExternalApplication
    {
        public static ThiaApplication Instance;
        internal static string ApplicationPath = "";
        private string TAB_NAME = "THIA";
        private string PANEL_NAME = "JSON Data";

        public BuilderFactory BuilderFactory { get; private set; }

        private void LoadTabAndRibbon(UIControlledApplication application)
        {
            try
            {
                application.GetRibbonPanels(TAB_NAME);
            }
            catch
            {
                application.CreateRibbonTab(TAB_NAME);
            }

            RibbonPanel toolsPanel = application.CreateRibbonPanel(TAB_NAME, PANEL_NAME);

            RibbonUtility ribbonUtility = new RibbonUtility(ApplicationPath, null);

            ribbonUtility.MountButton(typeof(CreateDocumentFromJSON), toolsPanel);
        }


        public Result OnStartup(UIControlledApplication application)
        {
            Instance = this;
            BuilderFactory = new BuilderFactory();

            ApplicationPath = Assembly.GetExecutingAssembly().Location;
            LoadTabAndRibbon(application);
            return Result.Succeeded;
        }


        public Result OnShutdown(UIControlledApplication a)
        {
            return Result.Succeeded;
        }
    }
}
