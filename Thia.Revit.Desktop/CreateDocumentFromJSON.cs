﻿using System;
using System.IO;

using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

using Thia.Revit.Core;
using Thia.Revit.Core.IO;
using Thia.Revit.Core.Extensions;

using Thia.Revit.Desktop.UI;


namespace Thia.Revit.Desktop
{
    [Mountable("Create Doc\r\nFrom JSON")]
    [Regeneration(RegenerationOption.Manual)]
    [Transaction(TransactionMode.Manual)]
    class CreateDocumentFromJSON : IExternalCommand, IExternalCommandAvailability
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            TaskDialog.Show("Input", "Select save location for new Revit project document.");

            string filePath = FileFolderBrowser.GetUserDefinedPath();

            Document newDoc = DocumentUtility.CreateProjectDocument(commandData.Application.Application, filePath, false);

            TaskDialog.Show("Input", "Select JSON file to create BIM from.");

            string jsonPath = FileFolderBrowser.GetFile();

            string jsonText = File.ReadAllText(jsonPath);

            JSONHandler jsonHandler = new JSONHandler(jsonText);


            //if (jsonHandler.IsValid == false)
            //{
            //    TaskDialog.Show("Error", "Could not validate JSON");
            //    return Result.Failed;
            //}

            InputElementDataCollection collection = jsonHandler.Parse<InputElementDataCollection>();

            if (collection == null)
            {
                TaskDialog.Show("Error", "Could not parse JSON");
                return Result.Failed;
            }

            foreach (InputElementData data in collection.Elements)
            {
                data.BIMObjectType = (BIMObjectType)Enum.Parse(typeof(BIMObjectType), data.ElemType.ToUpper());

                IBuilder bimObjectBuilder = ThiaApplication.Instance.BuilderFactory[data.BIMObjectType];

                bimObjectBuilder.SetData(data);

                newDoc.WithTransaction("Build From JSON", bimObjectBuilder.PreBuild, bimObjectBuilder.Build);
            }

            newDoc.SaveAs(filePath, new SaveAsOptions() { OverwriteExistingFile = true });

            commandData.Application.OpenAndActivateDocument(filePath);

            return Result.Succeeded;
        }


        public bool IsCommandAvailable(UIApplication applicationData, CategorySet selectedCategories) => true;
    }
}
